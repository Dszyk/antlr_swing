package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	//zmienne globalne
	private GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    } 

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	//dzielenie liczb
	protected Integer divide(Integer e1, Integer e2) {
		Integer out=0;
		//nadpisujemy nasz out wynikiem dzielenia
		try {
			out=e1/e2;
		}
		//wyjatek - gdy probujemy dzielic przez 0 pojawia nam sie komunikat
		catch(Exception e){
			System.out.println("Don't divide by 0");
			out=0;
		}
		return out;
	}
	//potegowanie liczb
	protected Integer exp(Integer e1, Integer e2) {
		Integer out=0;
		//nadpisujemy nasz out wynikiem potegowania
			out=(int) Math.pow(e1, e2);		
		
		return out;
	}
	
	//obsluga zmiennych globalnych
	protected void declareVar(String txt) {
		globalSymbols.newSymbol(txt);
	}

	protected void setVar(String txt, Integer i) {
		globalSymbols.setSymbol(txt, i);
	}
	protected Integer getVar(String txt) {
		return globalSymbols.getSymbol(txt);
	}
	
}
